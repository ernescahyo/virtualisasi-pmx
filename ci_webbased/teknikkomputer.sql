-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2014 at 06:50 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `teknikkomputer`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permalink` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `permalink`, `name`, `description`) VALUES
(7, 'tips', 'Tips', 'Berisi tentang tips-tips unik serta berguna yang kami sajikan untuk pembaca.'),
(8, 'kegiatan', 'Kegiatan', 'Menjelaskan kegiatan-kegiatan di Kampus Politama'),
(10, 'profil', 'Profil', 'Menggambarkan Profil Jurusan Teknik Komputer Politama'),
(11, 'akademik', 'Akademik', 'Penjelasan menganai kegiatan belajar mengajar, beasiswa dsb.'),
(12, '', 'Penelitian', 'Hasil tulisan dari Dosen Teknik Komputer Politama'),
(13, 'Kerjasama', 'Kerjasama', 'Berita tentang kerjasama jurusan teknik komputer dengan instansi lain'),
(14, '', 'Fasilitas', ''),
(15, '', 'Link', ''),
(16, '', 'Berita', ''),
(17, '', 'Lain Lain', ''),
(18, '', 'HMJ', 'Himpunan Mahasiswa Jurusan Teknik Komputer');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE IF NOT EXISTS `membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_depan` varchar(25) NOT NULL,
  `nama_belakang` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`id`, `nama_depan`, `nama_belakang`, `username`, `password`, `email_address`, `type`) VALUES
(2, 'Rhesa', 'Prawedha', 'drprawedha', '40a318cfbb4b4d458c86e5e50dc186db', 'dr.prawedha@gmail.com', 'admin'),
(8, 'Administrator', 'Teknik Komputer', 'Staff', 'f9600e6be2dd0aa8b695821f62b88aca', 'teknikkomputer@gmail.com', 'admin'),
(9, 'Staff', '2015', 'staff', '5d61a8bc9d4a7e1c0fc3d8c0161c542c', 'staff@teknikkomputer.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `created` date NOT NULL,
  `image` text NOT NULL,
  `body` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_posts_categories1` (`categories_id`),
  KEY `fk_posts_users1` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `permalink`, `created`, `image`, `body`, `status`, `categories_id`, `users_id`) VALUES
(5, 'Empat Cara Mengelola Ide', 'Empat-Cara-Mengelola-Ide', '2014-08-19', '', 'Tidak sedikit bisnis besar yang bermula dari ide-ide kecil.Tak bisa diduga, ide dapat muncul di mana saja dan dari siapa saja.Kadang ide muncul saat menjelang tidur, atau saat kita duduk santai di beranda rumah.Tidak jarang pula ide diperoleh melalui riset, focus group discussion, atau berbagai pendekatan ilmiah lainnya.Masalah yang muncul kemudian adalah bagaimana kita mengelola ide, terutama yang kita dapat &ldquo;cuma-cuma&rdquo;, agar tidak hilang atau terlupa begitu saja.  Berikut adalah beberapa cara yang dirilis Entrepreneur untuk mengelola ide dan mentransformasinya dalam sebuah tindakan nyata. Tentu saja sebuah ide akan percuma jika berupa mimpi yang tidak dapat diwujudkan.  Pertama, diskusikan ide di lokasi yang terpusat.Menciptakan sebuah tempat dengan iklim yang kondusif guna mendorong terciptanya ide sangat penting untuk dilakukan.Usahakan tempat ini memungkinkan orang-orang yang terlibat dapat saling bertukar pikiran dan berbagi feedback.  Kedua, berilah label untuk ide-ide yang berbeda. Hal ini akan memudahkan identifikasi serta tindak lanjut dari ide-ide yang didapat. Misalnya masukan dari konsumen dipilah dari ide-ide yang disampaikan pemasok atau distributor. Kategorisasi label dapat dilakukan sesuai dengan selera atau kebutuhan agar lebih efektif dan memudahkan pemilahan.  Ketiga, pastikan daftar ide diberikan pada orang yang tepat.Jika anda seorang pemimpin di perusahaan anda, pastikan ide-ide pilihan diberikan kepada orang yang tepat.Ide tentang desain mobil yang perlu diubah misalnya, harus disampaikan kepada bagian desainer atau engineer, bukan kepada bagian akunting, demikian pula sebaliknya.  Keempat, perlakukan daftar ide seperti kotak masuk email (inbox).Untuk menjaga ide-ide anda tidak terblokir, segera tindaklanjuti dengan respon yang tepat.Singkirkan ide-ide yang tidak memiliki nilai dan pilihlah yang memiliki potensi untuk dikembangkan. Beberapa ide mungkin butuh penyesuaian lebih lanjut, sementara ide lain mungkin dapat diimplementasikan dengan segera. Jadi jangan tunggu hingga lupa.', 1, 7, 1),
(6, 'Kunjungan Industri', 'Kunjungan-Industri', '2014-08-19', 'public/media/posts/01_(5).jpg', 'Kunjungan Industri Teknik Komputer Politama ke PT Moratel Jakarta, disini mahasiswa diterangkan mengenai kinerja manajemen koneksi FO yang disediakan oleh PT Moretelindo. dan Mahasiswa sangat Antusias memperhatikan pembicara menerangkan materi yang disampaikan. []rhesa', 1, 8, 1),
(7, 'Reuni Akbar', 'Reuni-Akbar', '2014-08-19', 'public/media/posts/CIMG4229.jpg', 'Reuni yang diadakan oleh Jurusan Teknik Komputer dan Sekrtaris (30/6) yang dihadiri oleh mahasiswa aktif dan para alumnus dari kedua jurusan tidak hanya memberikan kesan mendalam bagi peserta namun lebih dari itu akan adanya kelanjutan dengan dibentuknya Jaringan Alumnus yang akan menjembatani kedua belah pihak yakni antara mahasiswa dengan dunia kerja di masa depan. []rhesa', 1, 8, 1),
(8, 'Kunjungan ke Pustekom', 'Kunjungan-ke-Pustekom', '2014-08-19', 'public/media/posts/CIMG4298.jpg', 'Kunjungan Industri Mahasiswa Teknik Komputer &amp; Jurusan Manajemen Informatika ke Pustekkom Jakarta dalam Rangka Kunjungan Industri mendapat sambutan baik dari karyawan Pustekkom. Para mahasiswa memperhatikan dengan seksama bagaimana memproduksi siaran yang baik dan juga bagaimana siaran tersebut bisa mengudara baik melalui sinyal TV, Radio ataupun web Multimedia. []Rhesa', 1, 8, 1),
(9, 'PINGFEST 2013', 'PINGFEST-2013', '2014-08-19', 'public/media/posts/IMG_4188-001.jpg', 'Salah satu Mahasiswa Teknik Komputer Politama sebagai Koordinator Komunitas Tumblr-blog Solo yang menyemarakan Pingfest 2013 yang diselenggarakan oleh Himaster UNS 12-15 November 2013. yang disana tidak hanya berkumpul komunitas-komunitas sosial media namun juga disemarakkan oleh lomba-lomba proyek IT, coding, Game maupun Akustika. []Rhesa', 1, 13, 1),
(15, 'asdas', 'asdas', '2014-08-21', '', 'asdasdasdasda', 0, 7, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
