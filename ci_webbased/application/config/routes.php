<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['welcome'] = 'welcome';
$route['default_controller'] = 'pages/view';
	$route['pages/(:any)'] = 'pages/view/$1';
$route['404_override'] = '';

$route['login'] = 'login';
	$route['login/validate_credentials'] = 'login/validate_credentials';
	$route['login/signup'] = 'login/signup';
	$route['login/create_member'] = 'login/create_member';
	$route['login/logout'] = 'login/logout';

$route['site'] = 'site';
	$route['site/members_area'] = 'site/members_area';
	$route['site/is_logged_in'] = 'site/is_logged_in';
	$route['site/login'] = 'login';

$route['sites/posts'] = 'sites/posts';
	$route['sites/posts/index'] = 'sites/posts/index';
	$route['sites/posts/add'] = 'sites/posts/add';
	$route['sites/posts/edit'] = 'sites/posts/edit';
	$route['sites/posts/delete'] = 'sites/posts/delete';

$route['sites/categories'] = 'sites/categories';
	$route['sites/categories/index'] = 'sites/categories/index';
	$route['sites/categories/add'] = 'sites/categories/add';
	$route['sites/categories/edit'] = 'sites/categories/edit';
	$route['sites/categories/delete'] = 'sites/categories/delete';
	




/* End of file routes.php */
/* Location: ./application/config/routes.php */