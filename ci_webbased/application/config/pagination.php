<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Customizing the Pagination
$config['uri_segment'] = 3;
$config['num_links'] = 2;
$config['use_page_numbers'] = FALSE;
$config['page_query_string'] = FALSE;

// Customizing the First Link
$config['first_link'] = 'First';
$config['first_tag_open'] = '<div>';
$config['first_tag_close'] = '</div>';

//Customizing the Last Link
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<div>';
$config['last_tag_close'] = '</div>';
//Adding Enclosing Markup
$config['full_tag_open'] = '<div class="pagination-centered">';
$config['full_tag_close'] = '</div>';
//Customizing the "Next" Link
$config['next_link'] = 'Next';
$config['next_tag_open'] = '   ';
$config['next_tag_close'] = '   ';
//Customizing the "Previous" Link
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '   ';
$config['prev_tag_close'] = '   ';
/* End of file pagination.php */
/* Location: ./application/config/pagination.php */
