<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site extends CI_Controller {
	var $template = 'templates/template';

	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
	}

	function members_area()
	{

		$data['content'] = 'dashboard/webmaster/logged_in_area';
        $this->load->view($this->template, $data);
	}
	
	function another_page() // just for sample
	{
		echo 'good. you\'re logged in.';
	}
	
	function is_logged_in()
	{
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			$data['title'] = 'Dashboard';
			echo 'You don\'t have permission to access this page. <a href="../login">Login</a>';	
			die();		
			//$this->load->view('login_form');
		}		
	}
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */