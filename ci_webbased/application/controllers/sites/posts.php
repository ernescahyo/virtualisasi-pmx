<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Posts extends CI_Controller {

    var $template = 'templates/template';
    var $imagePath = 'public/media/posts/';
    var $status = array(
        0 => 'draft',
        1 => 'published'
    );	

    function __construct() {
		parent::__construct();
		$this->is_logged_in();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Posts_model');
        $this->load->model('Categories_model');
    }
	
	function index($p=0,$jppage=10){
		$config['base_url'] = site_url().'sites/posts/index';
		$config['total_rows'] = $this->Posts_model->getjrecord();
		$config['per_page'] = $jppage; 
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['use_page_numbers'] = FALSE;
		$this->pagination->initialize($config); 
		$data['hslquery']=$this->Posts_model->getpostpage($p,$jppage);
		$data['categories'] = $this->Categories_model->findList();
		$data['status'] = $this->Posts_model->status;
		$data['content'] = 'dashboard/posts/index';
        $this->load->view($this->template, $data);
	}
	
	//LAB PERCOBAAN
	

		
	function lab() {

		$query = $this->db->query("SELECT * FROM `posts` WHERE `categories_id` = 8 ORDER BY `posts`.`title` DESC");
		foreach($query->result() as $post){
			echo br()
			.anchor ("sites/posts/detail/".$post->permalink, $post->title)
			.nbs(). " - ".word_limiter($post->body,10).br();

		}
		if ($query->result() > 0) {
			return $query->result_array();
		}

    }
	
	function bycategory($p=1,$jppage=10){
		$this->load->library('pagination');
		$config['base_url'] = site_url().'sites/posts/bycategory/';
		$config['total_rows'] = $this->Posts_model->getjrecord();
		$config['per_page'] = $jppage; 
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['use_page_numbers'] = FALSE;
		$this->pagination->initialize($config); 
		$data['hslquery']=$this->Posts_model->getpostpage($p,$jppage);
		$data['categories'] = $this->Categories_model->findList();
		$data['status'] = $this->Posts_model->status;
		$data['content'] = 'dashboard/posts/index';
        $this->load->view($this->template, $data);
	}
	
    function detail($permalink = null) {
        if ($permalink == null) {
            redirect('sites/posts');
        } 
        $data['post'] = $this->Posts_model->findByPermalink($permalink);
        $data['content'] = 'dashboard/posts/detail';
        $this->load->view($this->template, $data);
    }
		
    function add() {
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('body', 'body', 'required|xss_clean');
        $this->form_validation->set_rules('categories_id', 'category', 'required|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|xss_clean');
        $this->form_validation->set_error_delimiters('', '<br/>');
		
        if ($this->form_validation->run() == TRUE) {

            $params = array(
                'title' => $this->input->post('title'),
                'permalink' => url_title($this->input->post('title')),
                'body' => $this->input->post('body'),
                'categories_id' => $this->input->post('categories_id'),
                'status' => $this->input->post('status'),
                /*'users_id' => $this->session->userdata('id'),*/
                'created' => date("Y-m-d H:i:s")
            );
            if ($_FILES['image']['error'] != 4) {
                $config['upload_path'] = $this->imagePath;
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['max_size']	= '1000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
                $this->load->library('upload', $config);

				if ( ! $this->upload->do_upload("image"))
				{
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('dashboard\posts\add', $error);
				}
				else
				{
					$image = $this->upload->data();
					$params['image'] = $this->imagePath . $image['file_name'];
				}
            }

            $this->Posts_model->create($params);
            $this->session->set_flashdata('success', 'Post created');
            redirect('sites/posts');
        }
        $data['categories'] = $this->Categories_model->findList();
        $data['status'] = $this->Posts_model->status;
        $data['content'] = 'dashboard/posts/add';
		$data['error']= ' ' ;
        $this->load->view($this->template, $data);
    }

    function edit($id = null) {

        if ($id == null) {
            $id = $this->input->post('id');
        }
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('body', 'body', 'required|xss_clean');
        $this->form_validation->set_rules('categories_id', 'category', 'required|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|xss_clean');
        $this->form_validation->set_error_delimiters('', '<br/>');
        if ($this->form_validation->run() == TRUE) {

            $params = array(
                'title' => $this->input->post('title'),
                'permalink' => url_title($this->input->post('title')),
                'body' => $this->input->post('body'),
                'categories_id' => $this->input->post('categories_id'),
                'status' => $this->input->post('status'),
                /*'users_id' => $this->session->userdata('id'),*/
                'created' => date("Y-m-d H:i:s")
            );
            if ($_FILES['image']['error'] != 4) {
                $config['upload_path'] = $this->imagePath;
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['max_size'] = '200000';
                $this->load->library('upload', $config);

                if ($this->upload->do_upload("image")) {
                    $image = $this->upload->data();
                    $params['image'] = $this->imagePath . $image['file_name'];
                }
            }

            $this->Posts_model->update($id, $params);
            $this->session->set_flashdata('success', 'Post edited');
            redirect('sites/posts');
        }

        $data['post'] = $this->Posts_model->findById($id);
        $data['categories'] = $this->Categories_model->findList();
        $data['status'] = $this->status;
        $data['content'] = 'dashboard/posts/edit';
        $this->load->view($this->template, $data);
    }

    function delete($id = null) {
        if ($id == null) {
            $this->session->set_flashdata('error', 'Invalid post');
            redirect('sites/posts');
        } else {
            $post = $this->Posts_model->findById($id);
            if (file_exists($post['image'])) {
                unlink($post['image']);
            }
            $this->Posts_model->destroy($id);
            $this->session->set_flashdata('success', 'Post deleted');
            redirect('sites/posts');
        }
    }
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'You don\'t have permission to access this page. <a href="login">Login</a>';	
			die();		
			//$this->load->view('login_form');
		}		
	}
}

/* End of file posts.php */
/* Location: ./application/controllers/admin/posts.php */