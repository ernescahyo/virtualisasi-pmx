<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categories extends CI_Controller {

    var $template = 'templates/template';

    function __construct() {
		parent::__construct();
		$this->is_logged_in();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
        $this->load->model('Categories_model');
        $this->load->model('Posts_model');
		}

	function index($p=0,$jppage=5){
		$config['base_url'] = site_url().'sites/categories/index';
		$config['total_rows'] = $this->Categories_model->getjrecord();
		$config['per_page'] = $jppage; 
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['use_page_numbers'] = FALSE;
		echo $this->pagination->create_links();
		$this->pagination->initialize($config); 
		
		$data["hslquery"]=$this->Categories_model->getcategorypage($p,$jppage);
		$data['content'] = 'dashboard/categories/index';
        $this->load->view($this->template, $data);
	}

    function add() {

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == TRUE) {
            $this->Categories_model->create();
            $this->session->set_flashdata('success', 'Category created');
            redirect('sites/categories');
        }
        $data['content'] = 'dashboard/categories/add';
        $this->load->view($this->template, $data);
    }

    function edit($id = null) {
        if ($id == null) {
            $id = $this->input->post('id');
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == TRUE) {
            $this->Categories_model->update($id);
            $this->session->set_flashdata('success', 'Category edited');
            redirect('sites/categories');
        }
        $data['category'] = $this->Categories_model->findById($id);
        $data['content'] = 'dashboard/categories/edit';
        $this->load->view($this->template, $data);
    }

    function delete($id = null) {
        if ($id == null) {
            $this->session->set_flashdata('error', 'Invalid category');
            redirect('dashboard/categories');
        } else {
            $articles = $this->Posts_model->findByCategoryId($id);
            if (!empty($articles)) {
                $this->session->set_flashdata('error', 'This category could not deleted cause have some articles');
            } else {
                $this->Categories_model->destroy($id);
                $this->session->set_flashdata('success', 'Category deleted');
            }
            redirect('sites/categories');
        }
    }
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'You don\'t have permission to access this page. <a href="login">Login</a>';	
			die();		
			//$this->load->view('login_form');
		}		
	}
}

/* End of file categories.php */
/* Location: ./application/controllers/admin/categories.php */
