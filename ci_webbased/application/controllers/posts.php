<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Posts extends CI_Controller {
	
	/**
	 * HALAMAN UTAMA
	 * Perlu ditambahkan pembagian 
	 * 1. POST untuk News 
	 * 2. POST untuk Blog
	 */
	 
    var $template = 'dashboard/template';
	var $template_detail = 'template_detail';
	
    function __construct() {
        parent::__construct();
        $this->load->model('Posts_model');
    }
	
	function index($p=1,$jppage=4){
		$this->load->library('pagination');
		$config['base_url'] = site_url().'sites/posts/index';
		$config['total_rows'] = $this->Posts_model->getjrecord();
		$config['per_page'] = $jppage; 
		$this->pagination->initialize($config); 
		
		$data["hslquery"]=$this->Posts_model->getpostpage($p,$jppage);
		$data['content'] = 'page/home';
        $this->load->view($this->template, $data);
	}
	
    function detail($permalink = null) {
        if ($permalink == null) {
            redirect('pages/home');
        }
        $data['post'] = $this->Posts_model->findByPermalink($permalink);
        
        $data['page'] = 'posts/detail';
        $this->load->view($this->template, $data);
    }
}

/* End of file posts.php */
/* Location: ./application/controllers/posts.php */
