<div class="row">
<div class="columns small-text-justify">
<div data-magellan-expedition="fixed">
  <dl class="sub-nav">
    <dd data-magellan-arrival="Visi"><a href="#Visi">Visi</a></dd>
    <dd data-magellan-arrival="Misi"><a href="#Misi">Misi</a></dd>
    <dd data-magellan-arrival="Tujuan"><a href="#Tujuan">Tujuan</a></dd>
  </dl>
</div>

<a name="Visi"></a>
<h3 data-magellan-destination="Visi">Visi program studi</h3>
<p>Pada tahun 2020 program studi Teknik Komputer menjadi pusat pengembangan sumber daya manusia di bidang teknologi Komputer yang profesional, berkualitas dan mandiri. </p>
<a name="Misi"></a>
<h3 data-magellan-destination="Misi">Misi program studi</h3>
<p>Menyelenggarakan pendidikan profesional di bidang teknologi Komputer, berbasis pada perkembangan DUDI, menghasilkan SDM profesional dan mampu berkompetisi di era global.</p>
<a name="Tujuan"></a>
<h3 data-magellan-destination="Tujuan">Tujuan program studi</h3>
<p>Mendidik dan melatih mahasiswa untuk dapat menguasai ilmu pengetahuan dan teknologi di bidang Komputer dari dasar sampai aplikasinya dan pengembangannya sehingga mampu bersaing di pasar kerja.</p>
<h3>Sasaran dan strategi pencapaian.</h3>

<h5>a.	Sasarannya:</h5>
<p>Menghasilkan tenaga ahli madya Teknik Komputer profesional yang mampu menembus dan bersaing di pasar kerja.  </p>
<ol>
  <li> Secara internal adalah dosen, toolman, karyawan/tenaga kependidikan serta para mahasiswa di Program Studi Teknik Komputer. Tahun 2015 semua dosen S2 dan tersertivikasi    </li>
  <li>Secara eksternal adalah : pengguna lulusan/dudi, para calon mahasiswa (siswa SMU atau SMK), home industry, atau instansi lainnya yang membutuhkan pendidikan dan pelatihan di bidang Teknik Komputer. </li>
</ol>
<h5>b.	Strategi pencapaian :</h5>
<ol>
  <li>  Meningkatkan kualitas sumber daya manusia melalui pelatihan dan studi lanjut.</li>
  <li> Mendorong dan memfasilitasi dosen untuk meningkatkan kualitas jabatan akademik    </li>
  <li> Meningkatkan sarana dan prasarana pembelajaran</li>
  <li>Melakukan review kurikulum setiap tiga tahun untuk menyesuaikan kebutuhan stakeholders</li>
  <li>Melaksanakan perkuliahan sesuai dengan kalender pendidikan yang telah ditetapkan sebelumnya</li>
  <li>Meningkatkan mutu dan jumlah penelitian dan pengabdian untuk peningkatan profesionalitas serta perolehan biaya dari berbagai sumber.    </li>
  <li>Memperluas kerja sama dengan pihak luar dalam rangka peningkatan pelaksanaan tridarma perguruan tinggi, pemasaran serta keberlanjutan. </li>
</ol>
</div>
</div>