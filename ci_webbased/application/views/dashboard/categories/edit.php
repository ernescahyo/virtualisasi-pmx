<div class="row">
<h3>Edit Category</h3>
<?php	$submit = (array(
		'type' => 'submit',
		'name' => 'kirim',
		'value' => 'KIRIM',
		'class' => 'button [radius round]')
	);?>
<?php echo validation_errors()
	.br()
	.form_open_multipart('sites/categories/edit')
	.form_hidden('id', $category['id'])
	.'Name : '
	.br()
	.form_input(
	array 	(
			'name' => 'name', 
			'value' => set_value
				(	'name', 
					isset($category['name']) ? $category['name'] : '')
			)
	)
	
	.'Description : '
	.br()
	.form_textarea(
	array	(
			'name' => 'description', 
			'value' => set_value
				(	'description',
					isset($category['description']) ? $category['description'] : '')
				)
			)
	.br()
	.form_submit($submit)
	.form_close(); 
	?>
</div>
