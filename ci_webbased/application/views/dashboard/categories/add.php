<div class="row">
<h3>Add Category</h3>
<?php	$submit = (array(
		'type' => 'submit',
		'name' => 'kirim',
		'value' => 'KIRIM',
		'class' => 'button [radius round]')
	);?>
<?php echo br()
	.validation_errors()
	.form_open_multipart('sites/categories/add')
	.br()
	.'Name : '
	.br()
	.form_input(
		array(
			'name' => 'name', 
			'value' => set_value('name')
			)
		)
	.br()
	.'Description: '
	.br()
	.form_textarea(
		array(
			'name' => 'description', 
			'value' => set_value('description')
			)
		)
	.br()
	.form_submit($submit)
	.form_close(); 
	?>
</div>
