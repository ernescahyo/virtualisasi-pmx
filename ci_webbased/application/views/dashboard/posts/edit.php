<div class="row">
<?=heading('Edit Page',2);?>
<?php	$submit = (array(
		'type' => 'submit',
		'name' => 'kirim',
		'value' => 'KIRIM',
		'class' => 'button [radius round]')
	);?>
<?php //echo initialize_tinymce(); ?>
<?php echo validation_errors()
	.form_open_multipart('sites/posts/edit')
	.form_hidden('id', $post['id'])
	.'Title :'
	.br()
	.form_input(
		array(
			'name' => 'title', 
			'value' => set_value(
				'title', 
				isset($post['title']) ? $post['title'] : '')
				)
			)
	.br()
	.'Body :'
	.br()
	.form_textarea(
		array(
			'name' => 'body', 
			'value' => set_value(
				'title', 
				isset($post['body']) ? $post['body'] : '')
				)
			)
	.br()
	.'Category :'
	.br()
	.form_dropdown(
		'categories_id', 
		$categories,
		$post['categories_id'])
	.br()
	.'Current Image : '
	.br();?>
	<?php if (!empty(
		$post['image'])): ?>       
            <img src="<?=base_url($post['image']);?>" width="100"/>
    <?php else: 
		img(array (
				'src' => 'public/images/no_image.jpg',
				'width'=> '100'
				)
			);
		endif; ?>
    <?=br()
	.'Image : '
	.br()
	.form_upload('image')
	.br()
	.'Status :'
	.br()
	.form_dropdown(
		'status', 
		$status, 
		isset($post['status']) ? $post['status'] : '')
	.br()
	.form_submit($submit)
	.form_close(); ?>
	</div>