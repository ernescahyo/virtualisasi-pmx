<div class="row">
<?php	$submit = (array(
		'type' => 'submit',
		'name' => 'kirim',
		'value' => 'KIRIM',
		'class' => 'button [radius round]')
	);?>
<?=heading('Add Post',2);?>
<?php //echo initialize_tinymce(); ?>
<?php echo validation_errors()
	.$error
	.form_open_multipart('sites/posts/add')
	.'Title :'
	.br()
	.form_input(
			array(
				'name' => 'title', 
				'value' => set_value(
					'title', 
						isset($page['title']) ? $page['title'] : ''
						)
					)
			)
	.br()
	.'Body :'
	.br()
	.form_textarea(
			array(
				'name' => 'body', 
				'value' => set_value(
					'title', 
						isset($page['body']) ? $page['body'] : ''
						)
					)
				)
	.br()
	.'Category :'
	.br()
	.form_dropdown('categories_id',$categories)
	.br()
	.'Image'
	.br()
	.form_upload ('image')
	.br()
	.'Status'
	.br()
	.form_dropdown(
		'status', 
		$status, 
		isset($page['status']) ? $page['status'] : '')
	.br()
	.form_submit($submit)
	.form_close(); ?>
</div>