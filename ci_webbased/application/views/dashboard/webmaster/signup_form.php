<div class="row">

<h1>Create an Account!</h1>

<fieldset>
<legend>Personal Information</legend>
<?php
$first_name = array( 
	"name" => "first_name"
	,"placeholder" => "First Name"
	,	
);
$last_name = array(
	"name" => "last_name"
	,"placeholder" => "Last Name"
	,	
);
$email_address = array(
	"name" => "email_address"
	,"placeholder" => "Email Address"
	,	
);
echo form_open('login/create_member');
echo form_input($first_name);
echo form_input($last_name);
echo form_input($email_address);
?>
</fieldset>

<fieldset>
<legend>Login Info</legend>
<?php
$submit = (array(
		'type' => 'submit',
		'name' => 'Create Acccount',
		'value' => 'Create Acccount',
		'class' => 'button [radius round]',)
		);

$username = array( 
	"name" => "username"
	,"placeholder" => "Username"
	,	
);
$password = array(
	"name" => "password"
	,"placeholder" => "Password"
	,	
);
$password2 = array(
	"name" => "password2"
	,"placeholder" => "Password Confirm"
	,"value" => ""
	,
);
echo form_input($username);
echo form_password($password);
echo form_password($password2);
echo form_submit($submit);
?>

<?php echo validation_errors('<p class="error">'); ?>
</fieldset>


</div>