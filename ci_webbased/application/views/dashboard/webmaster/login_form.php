<div class="row">	
    <h1>Login..</h1>
    <?php 
	$submit = (array(
		'type' => 'submit',
		'name' => 'Login',
		'value' => 'Login',
		'class' => 'button [radius round]',)
		);
	$username = array( 
	"name" => "username"
	,"placeholder" => "Username"
	,	
	);
	$password = array( 
	"name" => "password"
	,"placeholder" => "Password"
	,	
	);
	echo form_open('login/validate_credentials');
	echo form_input($username);
	echo form_password($password);
	echo form_submit($submit);
	echo br(1);
	echo anchor('login/signup', 'Create Account');
	echo form_close();
	?>
</div>