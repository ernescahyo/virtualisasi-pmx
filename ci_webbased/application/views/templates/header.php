<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title ?> Teknik Komputer | POLITAMA</title>
	<base href="<?php echo base_url();?>">
  <!-- If you are using the CSS version, only link these 2 files, you may add app.css to use for your overrides if you like -->
  <link rel="stylesheet" href="public/zurb/css/normalize.css">
  <link rel="stylesheet" href="public/zurb/css/foundation.css">
  <!--CSS Custom-->
  <link rel="stylesheet" href="public/css/app.css">
  <!-- If you are using the gem version, you need this only -->
  <link rel="stylesheet" href="public/zurb/css/app.css">
  <script src="public/zurb/js/vendor/modernizr.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>
<body>

	
    <nav class="top-bar" data-topbar>
  <ul class="title-area">
    <li class="name">
      <h1><a href="#"><b>TEKNIK KOMPUTER</b></a></h1>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
	<!-- Left Nav Section -->
    <ul class="left">
        <?='<li>'.anchor('','HOME').'</li>'?>
        <?='<li>'.anchor('pages/profil','PROFIL').'</li>';?>
        <?='<li>'.anchor('pages/akademik','AKADEMIK').'</li>';?>
        <?='<li>'.anchor('pages/penelitian','PENELITIAN').'</li>';?>
        <?='<li>'.anchor('pages/kerjasama','KERJASAMA').'</li>';?>
        <?='<li>'.anchor('pages/fasilitas','FASILITAS').'</li>';?>
        <?='<li>'.anchor('pages/peta','PETA').'</li>';?>
        <?='<li>'.anchor('pages/kontak','KONTAK').'</li>';?>
        <?='<li>'.anchor('pages/link','LINK').'</li>';?>
    </ul>
  </section>
</nav>
<nav class="breadcrumbs">
	<strong><?='Anda Berada di : '.set_breadcrumb().br(); ?></strong>
</nav>
