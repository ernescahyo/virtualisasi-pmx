<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard - Teknik Komputer | POLITAMA</title>
	<base href="<?php echo base_url();?>">
  <!-- If you are using the CSS version, only link these 2 files, you may add app.css to use for your overrides if you like -->
  <link rel="stylesheet" href="public/zurb/css/normalize.css">
  <link rel="stylesheet" href="public/zurb/css/foundation.css">
  <!--CSS Custom-->
  <link rel="stylesheet" href="public/css/app.css">
  <!-- If you are using the gem version, you need this only -->
  <link rel="stylesheet" href="public/zurb/css/app.css">
  <script src="public/zurb/js/vendor/modernizr.js"></script>

</head>
<body>



