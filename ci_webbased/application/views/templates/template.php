<base href="<?php echo base_url();?>">

<div class="off-canvas-wrap" data-offcanvas>
<div class="inner-wrap">
<nav class="tab-bar">
<section class="left-small">
<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
</section>

<section class="middle tab-bar-section">
<h1 class="title">Dashboard</h1>
</section>

</nav>

<aside class="left-off-canvas-menu">
<?=$this->load->view('templates/nav_dash');?>
</aside>


<section class="main-section">
<!-- content goes here -->
<?php $this->load->view('templates/header_dash');?>
<nav class="breadcrumbs">
	<strong><?='Anda Berada di : '.set_breadcrumb().br(); ?></strong>
</nav>
<div>
<?php if (!empty($content)): $this->load->view($content);
endif; ?>
</div>
<!-- close the off-canvas menu -->
<?php $this->load->view('templates/footer');?>
</section>

<a class="exit-off-canvas"></a>

</div>
</div>



