# PENDAHULUAN
Proyek terkait virtualisasi menggunakan Proxmox VE 5.0

# KONFIGURASI TERKAIT
## Ubuntu Server
1. Konfigurasi VSFTPD

## Debian Server
1. Apache Directory for All User (Untuk Konfigurasi Direktori Umum pada Pembelajaran Pemrograman Web)

## LXC (Owncloud)
1. Konfigurasi pada /usr/share/owncloud/config/config.php

## LXC E-Learning (Moodle)
__Dalam Pengembangan__

## Windows Client (Concurrent Mode)
__Dalam Pengembangan__

## CI_WebBased
__Sample Web Profile menggunakan CodeIgniter 2.X, dengan UI menggunakan Foundation CSS. (Project 2014)
