<?php
$CONFIG = array (
  'passwordsalt' => 'b471407a169c88c01d4b02760f0ebec7',
  'secret' => '5ef230eee525c805755c5a1c2f68ba828a1a25c0e5fa8eab30de7a72b438ee73$
  'trusted_domains' =>
  array (
    0 => 'localhost',
    1 => 'owncloud.stmik-aub.ac.id',
    2 => '192.168.1.20',
  ),
  'datadirectory' => '/usr/share/owncloud/data',
  'overwrite.cli.url' => 'http://owncloud.stmik-aub.ac.id',
  'dbtype' => 'mysql',
  'version' => '10.0.2.1',
  'dbname' => 'owncloud',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'owncloud',
  'dbpassword' => 'bd62c287c585487038b7290130642970',
  'logtimezone' => 'UTC',
  'installed' => true,
  'instanceid' => 'oc2kj3oydmx0',
);
